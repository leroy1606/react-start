import React from 'react';
import { Switch, Route, Link } from 'react-router-dom';

import Grid from 'material-ui/Grid';
import List, { ListItem, ListItemText } from 'material-ui/List';

//import glamorous from 'glamorous';

import './App.css';

import Home from '../Home/Home';
import Overview from '../Overview/Overview';
import About from '../About/About';
import Contact from '../Contact/Contact';
import NotFound from '../NotFound/NotFound';

// const LeftGrid = glamorous(Grid)({
//   backgroundColor: '#F5EEEC'
// });
// const RightGrid = glamorous(Grid)({
//   backgroundColor:'#F5EEEC',
// });

const App = () => (
  <div style={{ padding: 40 }}>
    <Grid container spacing={24} >
      <Grid item xs={2}>
        <List>
          <ListItem button component={Link} {...{to: "/"}}>
            <ListItemText primary="Home" />
          </ListItem>
          <ListItem button component={Link} {...{to: "/overview"}}>
            <ListItemText primary="Overview" />
          </ListItem>
          <ListItem button component={Link} {...{to: "/about"}}>
            <ListItemText primary="About" />
          </ListItem>
          <ListItem button component={Link} {...{to: "/Contact"}}>
            <ListItemText primary="Contact" />
          </ListItem>
        </List>
      </Grid>
      <Grid item xs={10}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/overview" component={Overview} />
            <Route path="/about" component={About} />
            <Route path="/contact" component={Contact} />
            <Route component={NotFound} />
          </Switch>
      </Grid>
    </Grid>
  </div>
)

export default App;
