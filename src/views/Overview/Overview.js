import React, { Component } from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {getLogs} from '../../actions/logsAction';

import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';

import LogDialog from '../../components/LogDialog'

class Overview extends Component{
    constructor(props) {
        super(props);
        this.state = { openLogDialog: false };

        this.toggleDialog = this.toggleDialog.bind(this);
    }

    toggleDialog = () => {
        this.setState({
            openLogDialog: !this.state.openLogDialog
        });
    };

    componentDidMount() {
        this.props.getLogs();
    }

    createTableItems(){
        return this.props.logsReducer.logs.length > 0 ? this.props.logsReducer.logs.map(log => (
            <TableRow key={log.id}>
                <TableCell>{log.id}</TableCell>
                <TableCell
                    // onClick={()=> this.props.selectLog(log)}
                >{log.title}</TableCell>
                <TableCell>{log.date}</TableCell>
                <TableCell>{log.startTime}</TableCell>
                <TableCell>{log.endTime}</TableCell>
                <TableCell>{log.comment}</TableCell>
            </TableRow>
        )) : null;
    }

    render(){
        return(
        <div>
            <Typography type="headline" component="h1">Overview Page</Typography>
            <Typography component="p">This is the Overview page.</Typography>
            <Paper>
                <Table>
                    <TableHead>
                    <TableRow>
                        <TableCell>id</TableCell>
                        <TableCell>Title</TableCell>
                        <TableCell>Date</TableCell>
                        <TableCell>Start time</TableCell>
                        <TableCell>End time</TableCell>
                        <TableCell>Comment</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.createTableItems()}
                    </TableBody>
                </Table>
            </Paper>
            <Button raised onClick={this.toggleDialog}>Add log</Button>
            <LogDialog open={this.state.openLogDialog} toggleDialog={this.toggleDialog}/>
        </div>
        );
    }
}

function mapStateToProps(state){
    return {
        logsReducer: state.logsReducer
    };
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({getLogs: getLogs},dispatch);
}

export default connect(mapStateToProps,matchDispatchToProps)(Overview)