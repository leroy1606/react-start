import axios from "axios";

const logs = (logs) => {
  return ({
    type: "GET_LOGS",
    payload:logs
  })
}

const postLogStatusAction = (newLog) => {
  console.log("postLogStatusAction: ", newLog)
  return ({
    type: "POST_LOG",
    payload:newLog
  })
}

export const getLogs = () => {
  return dispatch => {
    axios.get("http://localhost:4000/api/logs")
    .then(
      result => dispatch(logs(result.data))
    ).catch(
      error => console.error(error)  
    )

  }
}

export const postLog = (newLog) => {
  return dispatch => {
    axios.post("http://localhost:4000/api/logs",newLog)
    .then(
      result => {
        dispatch(getLogs())
        dispatch(postLogStatusAction(result.data))
      }
    ).catch(
      error => console.error(error)  
    )

  }
}



