import React, { Component } from 'react'
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {postLog} from '../actions/logsAction';

import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogTitle,
  } from 'material-ui/Dialog';

class LogDialog extends Component{
    constructor(props) {
        super(props);
        this.state = { title: "", date:"",startTime:"",endTime:"",comment:"" };
    }

    clearFormAndCloseModal = () => {
        this.setState({title: "", date:"",startTime:"",endTime:"",comment:""});
        this.props.toggleDialog();
    };

    postLog = () => {
        this.props.postLog({ 
            title: this.state.title, 
            date: this.state.date, 
            startTime: this.state.startTime, 
            endTime: this.state.endTime,
            comment: this.state.comment 
        });
        this.clearFormAndCloseModal();         
    };    

    render() {
        return(
        <Dialog open={this.props.open} onRequestClose={this.handleRequestClose}>
            <DialogTitle>Add log</DialogTitle>
            <DialogContent>
                <TextField autoFocus margin="dense" id="title" label="Title" fullWidth required
                    value={this.state.title}
                    onChange={e => this.setState({ title: e.target.value })}
                />
                <TextField autoFocus margin="dense" id="date" label="Date" fullWidth required
                    value={this.state.date}
                    onChange={e => this.setState({ date: e.target.value })}
                />
                <TextField autoFocus margin="dense" id="startTime" label="Start time" fullWidth
                    value={this.state.startTime}
                    onChange={e => this.setState({ startTime: e.target.value })} 
                />
                <TextField autoFocus margin="dense" id="endTime" label="End time" fullWidth
                    value={this.state.endTime}
                    onChange={e => this.setState({ endTime: e.target.value })} 
                />
                <TextField autoFocus margin="dense" id="comment" label="Comment" fullWidth
                    value={this.state.comment}
                    onChange={e => this.setState({ comment: e.target.value })}
                />
            </DialogContent>
            <DialogActions>
            <Button onClick={this.props.toggleDialog} color="primary">
                Cancel
            </Button>
            <Button onClick={this.postLog} color="primary">
                Add
            </Button>
            </DialogActions>
        </Dialog>
        )
    }
}

function mapStateToProps(state){
    return {
        //logsReducer: state.logsReducer,
    };
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({postLog: postLog},dispatch);
}

export default connect(mapStateToProps,matchDispatchToProps)(LogDialog)