const initialState = {
	logs: [],
	postLogStatus:{}
}

export const logsReducer = (state = initialState, action) => {
	switch (action.type) {
		case 'GET_LOGS':
			return {
				...state, 
				logs: action.payload
			}
		case 'POST_LOG':
			console.log("POST_LOG reducer: ",action.payload);
			return {
				...state, 
				postLogStatus: action.payload
			}
		default:
			return state
		}
}