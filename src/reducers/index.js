import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import {logsReducer} from './logsReducer';

export default combineReducers({
  router: routerReducer,
  logsReducer: logsReducer
});