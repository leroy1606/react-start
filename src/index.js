import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import './index.css';
import App from './views/App/App';
import registerServiceWorker from './registerServiceWorker';

import reducerCollection from './reducers';

import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';
const theme = createMuiTheme({
  palette: {
    //type: 'dark', // Switching the dark mode on is a single property value change.
  },
});

const history = createHistory();
const middleware = routerMiddleware(history);
export const store = createStore(
    reducerCollection,
    compose(
        applyMiddleware(middleware, thunk),
        window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() : f => f
    )
);

ReactDOM.render((
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <App />
      </div>
    </ConnectedRouter>
  </Provider>
  </MuiThemeProvider>
),document.getElementById('root'));
registerServiceWorker();
